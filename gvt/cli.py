# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/patrick.godwin/gvt/-/raw/main/LICENSE


import math
from pathlib import Path
from typing import Optional

from typer import Argument, Option, Typer

from dqsegdb2.query import query_segments
from gwpy.table import EventTable
from gwpy.timeseries import TimeSeries

from . import constants
from . import plots
from .fetch import EventType, fetch_events
from .validate import get_metadata, validate_gspy_events, validate_injections


gvt = Typer(help="Glitch Validation Toolkit")


@gvt.command()
def fetch(
    event_type: EventType = Argument(..., help="The type of event to get."),
    start: float = Argument("The GPS start time."),
    end: float = Argument("The GPS end time."),
    channel: str = Option(..., help="The channel to grab events from."),
    column: Optional[list[str]] = Option(
        None, help="If specified, the list of columns to query from."
    ),
    filter: Optional[list[str]] = Option(
        None, help="If specified, conditions in which to filter events."
    ),
    filter_times: Optional[str] = Option(
        None, help="Keep only times corresponding to this data quality flag."
    ),
    directory: Optional[Path] = Option(
        None,
        help=(
            "If specified, the base directory where events are located. "
            "Only relevant for snax events."
        ),
    ),
    output: Optional[Path] = Option(
        None,
        "--output",
        "-o",
        help=(
            "Write output to file. If not specified, writes a file to "
            "the current working directory with the T050017 naming "
            "convention: {detector}-{event_type.upper()}-{start}-{end}.h5"
        ),
        show_default=False,
    ),
) -> None:
    """Fetch events of a given type and write them to disk."""
    if filter_times:
        segments = query_segments(filter_times, start, end, coalesce=True)["active"]
    else:
        segments = None

    match event_type:
        case EventType.SNAX:
            events = fetch_events(
                event_type,
                channel,
                start,
                end,
                column,
                filter,
                segments=segments,
                directory=directory,
            )
        case _:
            events = fetch_events(
                event_type, channel, start, end, column, filter, segments=segments
            )

    if not output:
        detector, _ = channel.split(":")
        int_start = math.floor(start)
        int_end = math.ceil(end)
        duration = int_end - int_start
        output = Path(
            f"{detector}-{event_type.upper()}_EVENTS-{int_start}-{duration}.h5"
        )
    group = f"events/{channel}"
    events.write(str(output), format="hdf5", path=group)


@gvt.command()
def validate(
    target: Path = Option(
        ..., "--target", "-t", help="The target events to validate against."
    ),
    reference: Path = Option(
        ..., "--reference", "-r", help="The reference events for validation."
    ),
    coincidence_window: float = Option(
        constants.COINCIDENCE_WINDOW,
        help="The maximum time spacing to consider an event to be in coincidence.",
    ),
    snr_threshold: float = Option(
        constants.SNR_THRESHOLD,
        "--threshold",
        "--snr-threshold",
        help="The SNR threshold, used when validating against injections.",
    ),
    duration: float = Option(
        constants.EVENT_DURATION,
        "--duration",
        "--event-duration",
        help="The duration in seconds to plot around events.",
    ),
    limit: int = Option(
        10, "--limit", "-n", help="The maximum number of missed events to follow up."
    ),
    filter: Optional[list[str]] = Option(
        None, help="If specified, conditions in which to filter target events."
    ),
    output: Path = Option(
        Path.cwd(), "--output", "-o", help="Write output to this directory."
    ),
):
    """Validate a set of events against a reference."""
    reference_events = EventTable.read(reference)
    target_events = EventTable.read(target)

    reference_type = get_metadata(reference_events, "type")

    match EventType[reference_type.upper()]:
        case EventType.GSPY:
            validate_gspy_events(
                target_events,
                reference_events,
                output,
                coincidence_window,
                duration=duration,
                limit=limit,
                filters=filter,
            )
        case EventType.INJECTIONS:
            validate_injections(
                target_events,
                reference_events,
                output,
                coincidence_window,
                snr_threshold=snr_threshold,
                duration=duration,
                limit=limit,
                filters=filter,
            )
        case _:
            raise ValueError("reference type needs to be injections or gspy")


@gvt.command()
def plot(
    source: Path = Option(
        ..., "--source", "-s", help="source events to plot for given times"
    ),
    channel: str = Option(..., help="The channel to grab events from."),
    times: list[float] = Argument(..., help="Times to generate plots for."),
    duration: float = Option(
        constants.EVENT_DURATION,
        "--duration",
        "--event-duration",
        help="The duration in seconds to plot around events.",
    ),
    output: Path = Option(
        Path.cwd(), "--output", "-o", help="Write output to this directory."
    ),
):
    """Generate combined omega scan / event plots from a set of source events."""
    events = EventTable.read(source)
    for time in times:
        dt = duration / 2
        series = TimeSeries.get(channel, time - dt, time + dt)
        plots.qscan_timeseries(output, time, dt, series, events)
