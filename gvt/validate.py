# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/patrick.godwin/gvt/-/raw/main/LICENSE

from pathlib import Path
from collections.abc import Mapping
from typing import Optional

from astropy.table import join, setdiff
from astropy.table.operations import join_distance

from gwpy.table import EventTable
from gwpy.timeseries import TimeSeries

from . import constants
from . import plots
from .fetch import EventType


def validate_gspy_events(
    target: EventTable,
    reference: EventTable,
    path: Path,
    coincidence_window: float = constants.COINCIDENCE_WINDOW,
    duration: float = constants.EVENT_DURATION,
    limit: int = 10,
    filters: Optional[list[str]] = None,
) -> None:
    """Validate a set of events against a reference.

    Parameters
    ----------
    target : EventTable
        The target events to validate against.
    reference : EventTable
        The reference events for validation.
    path : Path
        The directory to write this plot to.
    coincidence_window : float
        The maximum time spacing to consider an
        event to be in coindicence. Default is 0.25 seconds.
    duration : float
        The duration in seconds to plot around events.
        Default is 8 seconds.
    limit : int
        The maximum number of missed events to plot for
        each category. Default is 10.
    filters : list[str]
        The list of column filters to apply to target events
        prior to matching, e.g. "snr >= 8".

    """
    if get_metadata(reference, "type") != EventType.GSPY.value:
        raise ValueError("only gspy events are allowed as a reference")

    if filters:
        events = target.filter(filters)
    else:
        events = target

    # match and summarize
    found, missed = find_matching_events(events, reference, coincidence_window)
    summary = summarize_found_by_category(found, reference)

    # generate per-label and performance plots
    plots.missed_found_by_label(path, summary)
    plots.roc_curve(path, found, summary)

    # follow up on missed events
    if not missed:
        return

    channel = get_metadata(reference, "channel")
    missed_labels = missed.group_by("ml_label")
    for key, events_by_label in zip(missed_labels.groups.keys, missed_labels.groups):
        label = key["ml_label"]
        if label == "No_Glitch":
            continue
        for event in events_by_label[:limit]:
            t0 = event["time_ref"]
            dt = duration / 2
            series = TimeSeries.get(channel, t0 - dt, t0 + dt)
            match EventType[get_metadata(target, "type").upper()]:
                case EventType.SNAX:
                    plots.qscan_timeseries(path, t0, duration, series, target)
                case EventType.OMICRON:
                    pass
                case _:
                    pass


def validate_injections(
    target: EventTable,
    reference: EventTable,
    path: Path,
    coincidence_window: float = constants.COINCIDENCE_WINDOW,
    snr_threshold: float = constants.SNR_THRESHOLD,
    duration: float = constants.EVENT_DURATION,
    limit: int = 100,
    filters: Optional[list[str]] = None,
) -> None:
    """Validate a set of events against a reference.

    Parameters
    ----------
    target : EventTable
        The target events to validate against.
    reference : EventTable
        The reference events for validation.
    path : Path
        The directory to write this plot to.
    coincidence_window : float
        The maximum time spacing to consider an
        event to be in coindicence. Default is 0.25 seconds.
    snr_threshold : float
        The SNR threshold. Default is 5.5.
    duration : float
        The duration in seconds to plot around events.
        Default is 8 seconds.
    limit : int
        The maximum number of missed events to follow up.
        Default is 100.
    filters : list[str]
        The list of column filters to apply to target events
        prior to matching, e.g. "snr >= 8".

    """
    if get_metadata(reference, "type") != EventType.INJECTIONS.value:
        raise ValueError("only injection events are allowed as a reference")

    if filters:
        events = target.filter(filters)
    else:
        events = target

    # find coincidences
    found, missed = find_matching_events(events, reference, coincidence_window)

    # generate summary plots
    plots.injected_vs_detected_snr(path, found, snr_threshold)
    plots.injected_vs_detected_frequency(path, found)
    plots.injection_snr_mismatch(path, found, snr_threshold)
    plots.injection_frequency_mismatch(path, found)
    plots.time_difference_histogram(path, found, coincidence_window)
    plots.missed_found_injections(path, found, missed)

    # follow up on missed events
    if not missed:
        return

    channel = get_metadata(reference, "channel")
    for event in missed[:limit]:
        t0 = event["time_ref"]
        dt = duration / 2
        series = TimeSeries.get(channel, t0 - dt, t0 + dt)
        match EventType[get_metadata(target, "type").upper()]:
            case EventType.SNAX:
                plots.qscan_timeseries(path, t0, duration, series, target)
            case EventType.OMICRON:
                pass
            case _:
                pass


def find_matching_events(
    target: EventTable, reference: EventTable, coincidence_window: float
) -> tuple[EventTable, EventTable]:
    """Find events which are time coincident.

    Parameters
    ----------
    target : EventTable
        The target events to validate against.
    reference : EventTable
        The reference events for validation.
    coincidence_window : float
        The maximum time spacing to consider an
        event to be in coindicence. Default is 0.25 seconds.

    Returns
    -------
    (EventTable, EventTable)
        The found and missed events, respectively.

    """
    valid_types = {EventType.GSPY.value, EventType.INJECTIONS.value}
    if get_metadata(reference, "type") not in valid_types:
        raise ValueError("only gspy events or injections are allowed as a reference")

    # find matches
    events = join(
        reference,
        target,
        keys="time",
        table_names=["ref", "target"],
        join_type="inner",
        join_funcs={"time": join_distance(coincidence_window)},
        metadata_conflicts="silent",
    )

    # keep the most significant event in each match
    events = events.cluster("time_id", "snr_target", 0.1)

    # replace metadata with join-specific metadata
    tables_by_name = {"target": target, "reference": reference}
    copy_metadata(events, tables_by_name, "type")
    copy_metadata(events, tables_by_name, "significance")

    # find missed events
    all_events = reference.copy()
    all_events.rename_column("time", "time_ref")
    missed = setdiff(all_events, events, keys="time_ref")

    return events, missed


def summarize_found_by_category(found: EventTable, reference: EventTable) -> EventTable:
    """Summarize the number of events found by label.

    Parameters
    ----------
    found : EventTable
        The events that were found.
    reference : EventTable
        The reference events for validation.

    Returns
    -------
    EventTable
        A summary of missed/found events by label.

    """
    if get_metadata(reference, "type") != EventType.GSPY.value:
        raise ValueError("only gspy events are allowed as a reference")

    # group matching events by category
    categories = reference.group_by("ml_label")
    found_by_group = found.group_by("ml_label")

    # create summary by category
    summary = EventTable(names=("label", "found", "total"), dtype=("S20", "i4", "i4"))
    copy_metadata(summary, reference, "type")
    for key, group in zip(categories.groups.keys, categories.groups):
        label = key["ml_label"]
        mask = found_by_group.groups.keys["ml_label"] == label
        summary.add_row((label, len(found_by_group.groups[mask]), len(group)))

    return summary


def get_metadata(table, key):
    return table.meta[key]


def copy_metadata(target, reference, key):
    if isinstance(reference, Mapping):
        target.meta[key] = {name: table.meta[key] for name, table in reference.items()}
    else:
        target.meta[key] = reference.meta[key]
