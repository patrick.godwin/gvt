# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/patrick.godwin/gvt/-/raw/main/LICENSE

from enum import Enum
import glob
import os
from pathlib import Path
from typing import Optional

from astropy.table import unique

from gwpy.table import EventTable
from gwpy.table.filters import in_segmentlist
from gwtrigfind import find_trigger_files
from ligo.segments import segmentlist


class Detector(str, Enum):
    H1 = "H1"
    L1 = "L1"
    V1 = "V1"


class EventType(str, Enum):
    GSPY = "gspy"
    INJECTIONS = "injections"
    OMICRON = "omicron"
    SNAX = "snax"


def fetch_events(event_type: EventType | str, *args, **kwargs) -> EventTable:
    """Fetch events of a given type.

    Parameters
    ----------
    event_type : EventType | str
        The type of event to get.
    channel : str
        The channel to grab events from.
    start : float
        The GPS start time.
    end : float
        The GPS end time.
    columns : list[str]
        The list of columns to query for.
        Default columns depend on the event type.
    filters : list[str]
        The list of column filters to apply to filter events,
        e.g. "ml_confidence >= 0.9".
    segments : segmentlist
        Filter events to only those that fall within these segments.
    **kwargs :
        Other type-specific keyword arguments.

    Returns
    -------
    EventTable
        The events.

    """
    if isinstance(event_type, str):
        event_type = EventType[event_type.upper()]

    match event_type:
        case EventType.GSPY:
            return fetch_gravityspy_events(*args, **kwargs)
        case EventType.OMICRON:
            return fetch_omicron_events(*args, **kwargs)
        case EventType.SNAX:
            return fetch_snax_events(*args, **kwargs)
        case EventType.INJECTIONS:
            raise NotImplementedError("injections are not currently fetchable")


def fetch_gravityspy_events(
    channel: str,
    start: float,
    end: float,
    columns: Optional[list[str]] = None,
    filters: Optional[list[str]] = None,
    segments: Optional[segmentlist] = None,
) -> EventTable:
    """Fetch events from GravitySpy.

    Parameters
    ----------
    channel : str
        The channel to grab events from.
    start : float
        The GPS start time.
    end : float
        The GPS end time.
    columns : list[str]
        The list of columns to query for.
        By default, the columns selected will include:
          * event_time
          * ml_label
          * ml_confidence
          * snr
          * peak_frequency
          * q_value
    filters : list[str]
        The list of column filters to apply to filter events,
        e.g. "ml_confidence >= 0.9".
    segments : segmentlist
        Filter events to only those that fall within these segments.

    Returns
    -------
    EventTable
        The GravitySpy events.

    Notes
    -----
    Login credentials are required to access the GravitySpy database. IGWN
    members with LIGO.ORG credentials can find the required credentials at
    https://secrets.ligo.org/secrets/144/.

    The query will look at the GRAVITYSPY_DATABASE_USER and
    GRAVITYSPY_DATABASE_PASSWD environment variables to access the database.

    """
    detector, channel_name = channel.split(":")
    if detector not in Detector._member_names_:
        raise ValueError(f"detector not valid, select from {Detector._member_names_}")

    selection = [
        f"ifo = {detector}",
        f"channel = '{channel_name}'",
        f"{start} <= event_time <= {end}",
    ]
    if filters:
        selection.extend(filters)

    if not columns:
        columns = [
            "event_time",
            "ml_label",
            "ml_confidence",
            "snr",
            "peak_frequency",
            "q_value",
        ]

    # fetch and cluster nearby events
    events = EventTable.fetch(
        "gravityspy",
        "glitches_v2d0",
        columns=columns,
        selection=selection,
        host="gravityspyplus.ciera.northwestern.edu",
    )
    events = events.cluster("event_time", "ml_confidence", 0.5)
    events = unique(events, "event_time")

    # rename columns for ease/consistency
    events.rename_column("event_time", "time")

    # filter by segments
    if segments:
        events = events.filter(("time", in_segmentlist, segments))  # type: ignore

    # add metadata
    events.meta["type"] = EventType.GSPY.value
    events.meta["channel"] = channel
    events.meta["significance"] = "ml_confidence"

    return events


def fetch_omicron_events(
    channel: str,
    start: float,
    end: float,
    columns: Optional[list[str]] = None,
    filters: Optional[list[str]] = None,
    segments: Optional[segmentlist] = None,
) -> EventTable:
    """Fetch events from Omicron.

    Parameters
    ----------
    channel : str
        The channel to grab events from.
    start : float
        The GPS start time.
    end : float
        The GPS end time.
    columns : list[str]
        The list of columns to query for.
        By default, the columns selected will include:
          * time
          * snr
          * frequency
          * q
    filters : list[str]
        The list of column filters to apply to filter events,
        e.g. "ml_confidence >= 0.9".
    segments : segmentlist
        Filter events to only those that fall within these segments.

    Returns
    -------
    EventTable
        The Omicron events.

    """
    detector = channel.split(":")[0]
    if detector not in Detector._member_names_:
        raise ValueError(f"detector not valid, select from {Detector._member_names_}")

    selection = [f"{start} <= time <= {end}"]
    if filters:
        selection.extend(filters)
    if segments:
        selection.append(("time", in_segmentlist, segments))  # type: ignore

    if not columns:
        columns = [
            "time",
            "snr",
            "frequency",
            "q",
        ]

    # find triggers on disk
    # NOTE: some Omicron files have permission issues, which we skip over
    cache = find_trigger_files(channel, "omicron", start, end, ext="h5")
    cache = [path for path in cache if os.access(path.replace("file://", ""), os.R_OK)]
    events = EventTable.read(
        cache, columns=columns, selection=selection, format="hdf5", path="triggers"
    )

    # add metadata
    events.meta["type"] = EventType.OMICRON.value
    events.meta["channel"] = channel
    events.meta["significance"] = "snr"

    return events


def fetch_snax_events(
    channel: str,
    start: float,
    end: float,
    columns: Optional[list[str]] = None,
    filters: Optional[list[str]] = None,
    segments: Optional[segmentlist] = None,
    directory: Optional[Path] = None,
) -> EventTable:
    """Fetch events from Omicron.

    Parameters
    ----------
    channel : str
        The channel to grab events from.
    start : float
        The GPS start time.
    end : float
        The GPS end time.
    columns : list[str]
        The list of columns to query for.
        By default, the columns selected will include:
          * time
          * snr
          * frequency
          * q
    filters : list[str]
        The list of column filters to apply to filter events,
        e.g. "ml_confidence >= 0.9".
    segments : segmentlist
        Filter events to only those that fall within these segments.
    directory : Path
        The base directory where features are located.

    Returns
    -------
    EventTable
        The SNAX events.

    """
    detector = channel.split(":")[0]
    if detector not in Detector._member_names_:
        raise ValueError(f"detector not valid, select from {Detector._member_names_}")

    selection = [f"{start} <= time <= {end}"]
    if filters:
        selection.extend(filters)
    if segments:
        selection.append(("time", in_segmentlist, segments))  # type: ignore

    if not columns:
        columns = [
            "time",
            "snr",
            "frequency",
            "q",
        ]

    # find events on disk
    if not directory:
        directory = Path("/home/idq/snax/production/online/*/features")
    pattern = directory / "*" / f"{detector}-SNAX_FEATURES-*-*.h5"
    cache = [path for path in glob.iglob(str(pattern))]
    events = EventTable.read(
        cache,
        channels=channel,
        columns=columns,
        selection=selection,
        format="hdf5.snax",
    )

    # add metadata
    events.meta["type"] = EventType.SNAX.value
    events.meta["channel"] = channel.rsplit("_", 2)[0]
    events.meta["significance"] = "snr"

    return events
