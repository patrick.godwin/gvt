## Authors

| Name                | Email                                 |
|---------------------|---------------------------------------|
| Patrick Godwin      | <patrick.godwin@ligo.org>             |

## Contributors

| Name                | Email                                 |
|---------------------|---------------------------------------|
| Sydney Chamberlin   | <syd.chamberlin@gmail.com>            |
