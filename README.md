<h1 align="center">gvt</h1>

<p align="center">Glitch Validation Toolkit</p>

<p align="center">
  <a href="https://git.ligo.org/patrick.godwin/gvt/-/pipelines/latest">
    <img alt="ci" src="https://git.ligo.org/patrick.godwin/gvt/badges/main/pipeline.svg" />
  </a>
  <a href="https://docs.ligo.org/patrick.godwin/gvt/">
    <img alt="documentation" src="https://img.shields.io/badge/docs-mkdocs%20material-blue.svg?style=flat" />
  </a>
</p>

---

## Installation

```
pip install git+https://git.ligo.org/patrick.godwin/gvt.git
```

## Features

TODO

## Quickstart

TODO
